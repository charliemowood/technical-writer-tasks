
# Creating A Replication Peer Via The VAST CLI

By the end of this section, a user will be able to create a Replication Peer via the VAST CLI and to use the relevant options with the introduced command.

**Note: Creating a peer means establishing a connection to a remote cluster that will be the destination peer.**

## What command to use to create a Replication Peer and what options can it take?

The basic command to create the Replication peer:

 `nativereplicationremotetarget create `

Options to accomplish full creation:


 | Option  |  Explanation |
|---|---|
|   `--name` |  Define remote Native Replication Name. |
|  `--password` |   Set up remote password.  |
| `--leading-vip`  |   Define leading Virtual IP address.  |
|  `--pool-id` | Set local VIP pool ID  |
| `--peer-certificate`  | Define certificate to use for authentication with the peer.  |

Full example of usage:

`nativereplicationremotetarget create --name RemoteVastCluster --password <string> --leading-vip <x.x.x.x> --pool-id <integer> --peer-certificate <string>`
