# Comments:

## Task 1: VAST_Cluster

I realize there is quite a bit of pre-requisites and configuration needed to get to this stage in the product and on the client side. I felt that this is out of scope for the task as would require a lot of further investigation but wanted to highlight I noticed that.

Not sure the command I came up with works just speculation based on the one provided from `rmda`, would need testing/verification with SME. I hope I am correct in thinking # at the front of command means run as admin.

## Task 3: Replication Peer

I would need to get an SME to verify the commands used and descriptions. And all use cases. Can you just pass the command one option at a time or do you need to always work with the command in a certain way? I suspect the former but I don't know it for certain.

The peer certificate I would ask exactly what parameter is required as no example was provided in the Json. Not sure if the note is required if it is going alongside the UI documentation. But I always consider that the user reading the CLI documentation will not read the UI documentation so maybe a redraft of the information common to UI and CLI and then split the two things.
