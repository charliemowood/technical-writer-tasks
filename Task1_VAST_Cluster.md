# How to mount a VAST Cluster view on a Linux machine to enable access to an Element Store path via NFS version 3 over TCP.

By the end of this section, the user will be able to enable a Linux user to access an Element Store path via NFS version 3 over TCP.

## Pre-requisites
TBD

## Configuration
TBD

## Mounting

When mounting a VAST Cluster view on a Linux system over TCP you will need to run a command similar to the one below:

`# mount -t nfs -o vers=3,proto=tcp x.x.x.x:/<exportpath> /mnt/<desiredlocationdir>/ `

Explanation of options and parameters:

| Options / Parameters  |  Explanation |
|---|---|
| `-t nfs` | Specifying the type. |
|`-o vers=3` | Specifying the version. (VAST Cluster supports NVSv3 and not NFSv4.)|
|`tcp` or `proto=tcp` | Specifying the protocol. |
|`x.x.x.x` |Specifying the IP in the [VIP](https://support.vastdata.com/hc/en-us/articles/4409619092498-Managing-Virtual-IP-Pools) in VAST Cluster `<exportpath>` is the name you have given to resource in the Element store. |
|`/mnt/<desiredlocationdir>` |Where the resource should mount to on your client machine.|
