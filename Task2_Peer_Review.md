1. Would more signposting at the beginning of each section be beneficial, by the end of this section the user will be able to / understand / be able to do etc? Also ould there be some kind of explanation of how these topics connect at the very beginning? This might not be relevant but for a non expert I am not 100% sure why these particular things have been put together, how they connect / overlap, are they different things, do all these things need to configured properly for the product to work and so on.

2. The piece sometimes reads as what the product does with a kind of informative character. Could it read more of like what the user would like to do? I.e. how do File and Directory Permission work in VAST storage? What is the VAST Authorization Flow? ... etc.

3. In the setcion `Files and Diretc Permission`, maybe a little more formatting could make the example clearer, i.e. example permission flow with a numbered list.

4. The `VAST Cluster Authorization Flow` subsetcion is complicated, I wonder if the product is stable enough that some kind of flowchart could be beneficial so the user could quickly see what happens. I wonder if this section is sometimes too descriptive of what the product is doing under the hood.

For example as the current text is:

```
The permissions are checked according to the relevant permissions checking algorithm and access is authorized depending on the result of this check. The algorithm that is used varies depending on which security flavor is set in the view policy applied to the specific view. See Permissions Checking is According to Security Flavor for details.
```

Could be:

```
The permissions are checked according to the relevant permissions.  See Permissions Checking is According to Security Flavor for more details.
```

5. On the whole, I found the writing clear and easy to follow so no obvious comments specific to phrasing etc.
